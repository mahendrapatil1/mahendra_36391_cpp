#include<iostream>
#include<string>
#include<fstream>
#include<sstream>
#include <algorithm>
#include<vector>
#define VEHICLE_FILE "vehicle.csv"
#define CUSTOMER_FILE "customer.csv"
using namespace std;
class vehicle {
private:
	string company;
	string model;
	string number;
	string vehicleNo;
public:
	vehicle(void) :
		company(" "), model(" "), number(" "), vehicleNo(" ") {
	}
	vehicle(string company, string model, string number, string vehicleNo) {
		this->company = company;
		this->model = model;
		this->number = number;
		this->vehicleNo = vehicleNo;
	}
	void setCompany(string &company) {
		this->company = company;
	}
	void setModel(string &model) {
		this->model = model;
	}
	void setNumber(string &number) {
		this->number = number;
	}
	void setvehicleNo(string &vehicleNo) {
		this->vehicleNo = vehicleNo;
	}
	string& getCompany(void) {
		return this->company;
	}
	string& getModel(void) {
		return this->model;
	}
	string& getNumber(void) {
		return this->number;
	}
	string& getvehicleNo(void) {
		return this->vehicleNo;
	}
	friend istream& operator>>(istream &cin, vehicle &other) {
		cout << "Enter Company of Vehicle : ";
		cin >> other.company;
		cout << "Enter Model of Vehicle : ";
		cin >> other.model;
		cout << "Enter mobile Number: ";
		cin >> other.number;
		cout << "Enter number Number: ";
		cin >> other.vehicleNo;
		return cin;
	}
	friend ostream& operator<<(ostream &cout, vehicle &other) {
		cout << other.company << ", " << other.model << ", " << other.number
				<< " " << other.vehicleNo << endl;
		return cout;
	}
	void acceptRecordsVehicle() {
		cout << "Enter Company of Vehicle : ";
		cin >> this->company;
		cout << "Enter Model of Vehicle : ";
		cin >> this->model;
		cout << "Enter Mobile Number: ";
		cin >> this->number;
		cout << "Enter Vehicle Number: ";
		cin >> this->vehicleNo;
	}
	void printRecordsVehicle() {
		cout << this->company << ", " << this->model << ", " << this->number
				<< ", " << this->vehicleNo << endl;
	}
};
class customer {
private:
	string address;
	string mobile;
	string name;
	vector<vehicle> vehicle_list;

public:
	customer(void) :
		address(" "), mobile(" "), name(" ") {
	}
	customer(string &address, string &mobile, string &name) {
		this->address = address;
		this->mobile = mobile;
		this->name = name;
	}

	void setAddress(string &address) {
		this->address = address;
	}
	void setMobile(string &mobile) {
		this->address = address;
	}
	void setName(string &name) {
		this->address = address;
	}
	void setVehicle(vehicle v) {
		this->vehicle_list.push_back(v);
	}
	string& getAddress(void) {
		return this->address;
	}
	string& getMobile(void) {
		return this->mobile;
	}
	string& getName(void) {
		return this->name;
	}
	vector<vehicle>& getVehicles() {
		return this->vehicle_list;
	}
	friend istream& operator>>(istream &cin, customer &other) {
		cout << "Enter Address of customer	: ";
		cin >> other.address;
		cout << "Enter Mobile Number		: ";
		cin >> other.mobile;
		cout << "Enter Customer Name		: ";
		cin >> other.name;
		return cin;
	}
	friend ostream& operator<<(ostream &cout, customer &other) {
		cout << other.address << ", " << other.mobile << ", " << other.name
				<< endl;
		return cout;
	}
	void acceptRecordCustomer() {
		cout << "Enter Address of customer	: ";
		cin >> this->address;
		cout << "Enter Mobile Number		: ";
		cin >> this->mobile;
		cout << "Enter Customer Name		: ";
		cin >> this->name;
	}
	void printRecordsCustomer() {
		cout << this->address << ", " << this->mobile << ", " << this->name
				<< endl;
		for (unsigned i = 0; i < this->vehicle_list.size(); i++)
			this->vehicle_list[i].printRecordsVehicle();
	}
};

class Part {
private:
	string desc;
	double rate;
public:
	Part(void) :
		desc(" "), rate(0) {
	}
	Part(string &desc, double rate) {
		this->desc = desc;
		this->rate = rate;
	}
	void setDesc(string &desc) {
		this->desc = desc;
	}
	void setRate(double rate) {
		this->rate = rate;
	}
	string getDesc(void) {
		return this->desc;
	}
	double getRate() const {
		return this->rate;
	}
	void acceptRecord() {
		cout << "Enter Parts Description";
		cin >> this->desc;
		cout << "Enter Parts Rate";
		cin >> this->rate;
	}
	friend istream& operator>>(istream &cin, Part &other) {
		cout << "Enter Parts Description :";
		cin >> other.desc;
		cout << "Enter Parts Rate";
		cin >> other.rate;
		return cin;
	}
};

class service {
private:
	string desc;
public:
	service(void) :
		desc(" ") {
	}
	service(string &desc) {
		this->desc = desc;
	}
	void setDesc(string &desc) {
		this->desc = desc;
	}
	string getDesc(void) {
		return this->desc;
	}
	// virtual double price(  ) = 0;

	virtual void acceptRecord() {
		cout << "Enter Description : ";
		cin >> desc;
	}
	virtual void printRecord() {
		cout << "Description is : " << this->desc << endl;
	}

};
class maintenance: public service {
private:
	double labourcharge;
	vector<Part> part_list;
public:
	maintenance(void) :
		labourcharge(0.0) {
	}
	maintenance(string &desc, double labourcharge) {
		this->labourcharge = labourcharge;
	}
	void setLabourCharge(double labourcharge) {
		this->labourcharge = labourcharge;
	}
	double getLabourCharge(void) {
		return this->labourcharge;
	}
	void setPart(Part p) {
		this->part_list.push_back(p);
	}
	void partRate(vector<Part> rate) {

	}
	void addPart(Part &new_parts) {
		new_parts.acceptRecord();
		this->part_list.push_back(new_parts);
		cout << "Part Added" << endl;
	}
	virtual void acceptRecord(void) {
		service::acceptRecord();
		cout << "Enter Labour cost :";
		cin >> labourcharge;
	}
	virtual void printRecord(void) {
		service::printRecord();
		cout << "Labour charge is : " << labourcharge << endl;
	}
	virtual void price(double price) {

	}
};

class oil: public service {
private:
	double cost;
public:
	oil(void) :
		cost(0.0) {
	}
	oil(string &desc, double cost) {
		this->cost = cost;
	}
	void setCost(double cost) {
		this->cost = cost;
	}
	double getCost(void) {
		return this->cost;
	}
	virtual double price(void) {
		return this->cost;
	}
	virtual void acceptRecord(void) {
		service::acceptRecord();
		cout << "Enter oil cost";
		cin >> cost;
	}
	virtual void printRecord(void) {
		service::printRecord();
		cout << "oil cost is : " << cost << endl;
	}

};
class service_request {
private:
	string name;
	string vehicleNo;
	vector<service*> service_list;
public:
	service_request(void) :
		name(" "), vehicleNo(" ") {
	}
	service_request(string name, string vehicleNo) {
		this->name = name;
		this->vehicleNo = vehicleNo;
	}
	void processRequest() {
		service *serv = NULL;
		this->addItem(serv);
	}
	int menu_list() {
		int choice;
		cout << "0.Exit" << endl;
		cout << "1.Maintainace" << endl;
		cout << "2.Oil" << endl;
		cout << "Enter Your Choice" << endl;
		cin >> choice;
		return choice;
	}
	void addItem(service* serv) {
		int choice;
		while ((choice = menu_list()) != 0) {
			switch (choice) {
			case 1:
				serv = new maintenance();
				break;
			case 2:
				serv = new oil();
			}
			if (serv != NULL) {
				serv->acceptRecord();
				this->service_list.push_back(serv);
			}
			for (unsigned i = 0; i < this->service_list.size(); ++i)
				this->service_list[i]->printRecord();
		}
	}
	const string& getName() const {
		return name;
	}
	const vector<service*>& getServiceList() const {
		return service_list;
	}
	const string& getVehicleNo() const {
		return vehicleNo;
	}
	void setVehicleNo(const string& vehicleNo) {
		this->vehicleNo = vehicleNo;
	}
	void setName(const string& name) {
		this->name = name;
	}
};
class bill {
private:
	double amount;
	double paid_amount;

public:

	bill(void) :
		amount(0), paid_amount(0) {
	}
	bill(double amount, double paid_amount) {
		this->amount = amount;
		this->paid_amount = paid_amount;
	}
	/*
	 double coumputeAmount(  )
	 {
	 // service + maintanace + oil
	 }

	 double computeTax(  )
	 {
	 // coumputeAmount * 12.6 %
	 }

	 double computeTotalBill()
	 {
	 // coumputeAmount + computeTax
	 }
	 */
	void printRecord() {
		cout << "Your amount is : " << this->amount << ", "
				<< " Paid amount is : " << this->paid_amount << endl;
	}

	double getAmount(void) const {
		return amount;
	}

	void setAmount(double amount) {
		this->amount = amount;
	}

	double getPaidAmount(void) const {
		return paid_amount;
	}

	void setPaidAmount(double paidAmount) {
		paid_amount = paidAmount;
	}
};

class Servicestation {
private:
	vector<bill> bill_list;
	vector<customer> cust_list;
	string name;

public:

	Servicestation(void) :
		name(" ") {
	}

	Servicestation(string &name) {
		this->name = name;
	}

	Servicestation(const string& name) {
		this->name = name;
	}
public:
	static Servicestation& getInstance(void) {
		static Servicestation instance;
		return instance;
	}
	/*	double computeCash()
	 {

	 }
	 */
	void displayCustomerList() {
		for (unsigned i = 0; i < cust_list.size(); i++) {
			cout << cust_list[i].getName() << "," << cust_list[i].getMobile()
									<< "," << cust_list[i].getAddress() << endl;
		}
	}

	void loadcustomercsv() {
		ifstream fin("customer.csv");
		string line;
		while (getline(fin, line)) {
			string tokens[3];
			stringstream str(line);
			for (int i = 0; i < 3; i++)
				getline(str, tokens[i]);
			customer c(tokens[0], tokens[1], tokens[2]);
			cust_list.push_back(c);
		}
	}

	const vector<bill>& getBillList() const {
		return bill_list;
	}

	void setBillList(const vector<bill> &billList) {
		bill_list = billList;
	}

	const vector<customer>& getCustList() const {
		return cust_list;
	}

	void setCustList(const vector<customer> &custList) {
		cust_list = custList;
	}

	const string& getName() const {
		return name;
	}

	void setName(const string &name) {
		this->name = name;
	}
};

void loadCustomer(vector<customer> &customers) {
	ifstream fp, fpvehicles;
	string line;
	int i;
	fp.open(CUSTOMER_FILE);
	if (!fp) {
		perror("failed to open customers file");
		return;
	}
	i = 0;
	while (getline(fp, line)) {

		stringstream str(line);
		string tokens[3];
		for (int j = 0; j < 3; j++)
			getline(str, tokens[j]);
		customer c(tokens[0], tokens[1], tokens[2]);
		cout << c.getMobile();
		{
			fpvehicles.open(VEHICLE_FILE);
			if (!fpvehicles) {
				perror("failed to open vehicles file");
				return;
			}
			while (getline(fpvehicles, line)) {
				stringstream str(line);
				string tokens[4];
				for (int j = 0; j < 4; j++)
					getline(str, tokens[i]);
				vehicle v(tokens[0], tokens[1], tokens[2], tokens[3]);

				if (c.getMobile() == tokens[2]) {
					c.setVehicle(v);
				}
				i++;
			}
			fpvehicles.close();
		}
		customers.push_back(c);
		i++;
	}
	fp.close();
	cout << "-->Customer Loaded." << endl;
}

void storeCutomeres(vector<customer>& customers) {
	ofstream fp, fpvehicles;
	unsigned int i = 0, j = 0;
	fp.open(CUSTOMER_FILE);
	fpvehicles.open(VEHICLE_FILE);
	if (!fp) {
		cout << "failed to open customers file";
		return;
	}
	if (!fpvehicles) {
		cout << "failed to open vehilcles file";
		return;
	}

	for (i = 0; i < customers.size(); i++) {
		fp << customers[i].getAddress() << ", " << customers[i].getMobile()
								<< ", " << customers[i].getName() << endl;
		vector<vehicle> temp = customers[i].getVehicles();
		for (j = 0; j < temp.size(); j++) {
			fpvehicles << temp[j].getCompany() << ", " << temp[j].getModel()
									<< ", " << temp[j].getNumber() << ", "
									<< temp[j].getvehicleNo() << endl;
		}
	}
	fpvehicles.close();
	fp.close();
	cout << "--->Customer Saved.\n" << endl;
}

int menu_list() {
	int choice;
	cout << "0.Exit" << endl;
	cout << "----New Customer--------" << endl;
	cout << "1.Add New Customer" << endl;
	cout << "\n----New Servicing Request (for Existing Customer)-------"
			<< endl;
	cout << "2.Choose Customer " << endl; // customer vehicle we have to show add new vehicele if have and store it

	cin >> choice;
	return choice;
}
int main() {
	vector<customer> customers;
	vector<vehicle> vehicle_list;
	customer c1;
	vehicle v1;
	int choice, i, count;
	string num;
	while ((choice = menu_list()) != 0) {
		switch (choice) {
		case 1:
			c1.acceptRecordCustomer();
			loadCustomer(customers);
			customers.push_back(c1);
			storeCutomeres(customers);
			break;

		case 2:
			cout << "How many Vehicles you want to add....";
			cin >> count;


			if(count == 2 )
			{
				for (i = 0; i < count; i++)
				{
					v1.acceptRecordsVehicle();
					v1.setNumber(c1.getMobile());
					c1.setVehicle(v1);
					customers.push_back(c1);

					cout << "=====================================" << endl;
					for (unsigned i = 0; i < customers.size(); i++)
						customers[i].printRecordsCustomer();
					cout << "=====================================" << endl;
				}
				storeCutomeres(customers);
			} else if(i == 3 )
			{
				service_request sr;
				cout << "Enter Service you want to Take :" << endl;
				sr.processRequest();
				//Prepare bill and display it.
				//Get bill payment from customer and show balance.
			}
			else
			{
				cout << "Cutomer Not found" << endl;
				c1.acceptRecordCustomer();
			}
		}
		break;
	}
}

int main1() {
	int count, i;
	vector<customer> customers;
	vector<vehicle> vehicle_list;
	customer c1;
	vehicle v1;

	c1.acceptRecordCustomer();
	loadCustomer(customers);
	cout << "How many Vehicles you want to add....";
	cin >> count;
	for (i = 0; i < count; i++) {
		v1.acceptRecordsVehicle();
		v1.setNumber(c1.getMobile());
		c1.setVehicle(v1);
		customers.push_back(c1);

		cout << "=====================================" << endl;
		for (unsigned i = 0; i < customers.size(); i++)
			customers[i].printRecordsCustomer();
		cout << "=====================================" << endl;
	}
	storeCutomeres(customers);
	return 0;
}
