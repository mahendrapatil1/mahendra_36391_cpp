

#include<cstring>
#include<iostream>
using namespace std;

class String
{
private:
	size_t length;
	char *buffer;
public:
	String( void ) : length( 0 ), buffer( NULL )
{       }
	String( const char *str  ) : length( strlen(str) ), buffer( new char[ this->length + 1 ] )
	{
		strcpy( this->buffer, str );
	}
	String( size_t length )
	{
		this->length = length;
		this->buffer = new char[ this->length + 1 ];
	}
	String( const String &other  ) : length( other.length ), buffer( new char[ this->length + 1 ] )
	{
		strcpy( this->buffer, other.buffer );
	}

	String operator+( String &other )
	{
		String temp( this->length + other.length );
		strcpy(temp.buffer, this->buffer);
		strcat(temp.buffer, other.buffer);
		return temp;
	}

	friend ostream& operator<<( ostream &cout, const String &other )
	{
		cout<<other.buffer<<endl;
		return cout;
	}
	void operator~( void )
	        		{
		int i = 0, j = this->length - 1;
		while( i < j )
			swap(this->buffer[ i ++], this->buffer[ j -- ] );
	        		}
	~String( void )
	{
		if( this->buffer != NULL )
		{
			delete[] this->buffer;
			this->buffer = NULL;
		}
	}


};

int main( void )
{
	String s1("SunBeam");
	String s2("Karad");
	cout<<s1;
	cout<<s2;
	String s3 = s1 + s2;
	cout<<s3<<endl;
	~s3;
	cout<<s3;

	return 0;
}
