#include<iostream>
#include<string>
using namespace std;
class IllegalException
{
private:
	string message;
public:
	IllegalException( const string message )throw( ):message(message)
              {          }
	string getMessage( void )const throw( )
	{
		return this->message;
	}
};
class Account{
private:
	string name;
	int acnt_number;
	string type;
	int balance;
public:
	Account()throw( );
	void accept_record()throw( );
	void print_record()throw( );
	void set_name(const string name)throw( );
	void set_acnt_number(const int acnt_number)throw( );
	void set_type(const string type)throw( );
	void set_balance(int balance)throw( );
	string get_name()const throw( );
	int get_acnt_number()const throw( );
	string get_type()const throw( );
	int get_balance()const throw( );

};

class Bank{
private:
	int acnt_index;
	Account arr[5];
public:
	Bank(){
		acnt_index=0;

	}
	void set_acnt_index(const int acnt_index)throw( ){
		this->acnt_index=acnt_index;
	}
	void set_accounts()throw( ){
		//		this->accounts.insert

	}
	void accept_account_info()throw( );
	int create_account()throw( );
	float deposit( int number, float amount )throw( IllegalException );
	float withdraw(int number, float amount )throw( IllegalException );
	void get_account_details(int accNumber)throw( IllegalException );
	//	void accept_account_number();
	//	void accept_amount();


};
int count=1000;
Account::Account()throw( ){
	this->name = ("");
	this->acnt_number=0;
	this->type=("");
	this->balance=0;
}
void Account::accept_record()throw( ){
	cout<<"Enter your name:         "<<endl;
	cin>>name;
	cout<<"Enter account type:      "<<endl;
	cin>>type;
	cout<<endl;
}
void Account::print_record()throw( ){
	cout<<"Your name:       		"<<this->name<<endl;
	cout<<"Your account number:     "<<this->acnt_number<<endl;
	cout<<"Your account type:       "<<this->type<<endl;
	cout<<"Your balance:    		"<<this->balance<<endl;
}
void Account::set_name(const string name)throw( ){
	this->name=name;
}
void Account::set_acnt_number(const int acnt_number)throw(  ){
	this->acnt_number=acnt_number;
}
void Account::set_type(const string type)throw( ){
	this->name=type;
}
void Account::set_balance(const int balance)throw( ){

	this->balance=balance;
}
string Account::get_name()const throw( ){
	return name;
}
int Account::get_acnt_number()const throw( ){
	return acnt_number;
}
string Account::get_type()const throw( ){
	return type;
}
int Account::get_balance()const throw( ){
	return balance;
}


void Bank::accept_account_info( )throw( )
{
	this->arr[++acnt_index].accept_record();
}
int Bank::create_account()throw( )
{
	int account;
	this->arr[acnt_index].set_acnt_number(++count);
	account=this->arr[acnt_index].get_acnt_number();
	return account ;
}
float Bank::deposit( int number, float amount )throw( IllegalException )
{
	if ( amount<0)
		throw IllegalException (" Amount can not be Negative");

	for( int index = 0; index <= this->acnt_index; ++ index )
	{
		if(this->arr[ acnt_index ].get_acnt_number()  == number  )
		{       int balance=this->arr[ acnt_index ].get_balance();
		balance=balance+amount;
		this->arr[ this->acnt_index ].set_balance(balance);
		return this->arr[ acnt_index ].get_balance();
		}
	}
	return 0;
}
float Bank::withdraw(int number, float amount )throw( IllegalException )
{

	if ( number < 0 )
		throw IllegalException (" Invalid Account Number");
	if ( amount<0)
		throw IllegalException (" Amount can not be Negative");

	for( int index = 0; index <= this->acnt_index; ++ index )
	{
		if(this->arr[ acnt_index ].get_acnt_number()  == number  )
		{int balance=this->arr[ acnt_index ].get_balance();
		balance=balance-amount;
		this->arr[ this->acnt_index ].set_balance(balance);
		return this->arr[ acnt_index ].get_balance();
		}
	}
	return 0;
}
void Bank::get_account_details(int accNumber)throw( IllegalException )
{
	for(int index = 0; index <= this->acnt_index; ++ index){
	if(this->arr[this->acnt_index].get_acnt_number()==accNumber){
		this->arr[this->acnt_index].print_record();}
}
}

int menu_list( void )
{
	int choice;
	cout<<endl;
	cout<<"0.Exit"<<endl;
	cout<<"1.Create New Account"<<endl;
	cout<<"2.Deposit"<<endl;
	cout<<"3.Withdraw"<<endl;
	cout<<"4.Print Account details"<<endl;
	cout<<"Enter choice     :       "<<endl;
	cin>>choice;
	return choice;
}
int accept_account_number()
{       int number;
cout<<"Enter account number"<<endl;
cin>>number;
return number;
}
float accept_amount()throw ( IllegalException )
{
float amount;
cout<<"Enter amount"<<endl;
cin>>amount;
return amount;
}
int main( void )
{
	int choice, accNumber;
	float amount;
	Bank bank;
	bank.set_acnt_index(-1);
	Account account;
	while( ( choice = menu_list( ) ) )
	{
		try {
			switch( choice )
					{
					case 1:
						bank.accept_account_info();
						accNumber=bank.create_account();
						cout<<accNumber;
						break;
					case 2:
						accNumber=accept_account_number();
						amount=accept_amount();
						bank.deposit(accNumber,amount );
						bank.get_account_details(accNumber);
						break;
					case 3:
						accNumber=accept_account_number();
						amount=accept_amount();
						bank.withdraw(accNumber,amount );
						bank.get_account_details(accNumber);
						break;
					case 4:
						bank.get_account_details(accNumber);
			}

		}
		catch( IllegalException &ex)
		{
			cout<<ex.getMessage()<<endl;
		}
		catch ( ... )
		{
			cout<<"Exception"<<endl;
		}

	}
	return 0;
}
