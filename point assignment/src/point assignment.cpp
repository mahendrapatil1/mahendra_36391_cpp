#include <iostream>
using namespace std;

class point
{
private:
    int xposition;
    int yposition;
 public:
    point():xposition(0),yposition(0)
    {}
    point(int xposition,int yposition)
    {
        this->xposition=xposition;
        this->yposition=yposition;
    }
    void setXposition(int xposition)
    {
        this->xposition=xposition;
    }
    void setYposition(int yposition)
    {
        this->yposition=yposition;
    }
    int getXposition()
    {
        return this->xposition;
    }
    int getYposition()
    {
        return this->yposition;
    }
};
void accept(point *p)
{
    int x,y;
    cout<<"enter xposition  :   "<<endl;
    cin>>x;
    p->setXposition(x);

    cout<<"enter yposition  :   "<<endl;
    cin>>y;
    p->setYposition(y);
}
void display(point p)
{
    cout<<"xpositon   :   "<<p.getXposition()<<endl;
    cout<<"ypositon   :   "<<p.getYposition()<<endl;
}
void modify(point *p)
{
    int x,y;
    cout<<"enter modified value of xposition"<<endl;
    cin>>x;
    p->setXposition(x);

    cout<<"enter modified value of yposition"<<endl;
    cin>>y;
    p->setYposition(y);

}
int menu_list()
{
    int choice;
    cout<<endl;
    cout<<"0.exit"<<endl;
    cout<<"1.accept"<<endl;
    cout<<"2.display"<<endl;
    cout<<"3.modify"<<endl;
    cout<<"enter your choice"<<endl;
    cin>>choice;
    return choice;
}
int main()
{
    int choice;
    point  p(0,0);
    while((choice=::menu_list())!=0)
    {
        switch(choice)
        {
        case 1:
            accept(&p);
            break;
        case 2:
            display(p);
            break;
        case 3:
            modify(&p);
            break;
        }
    }

    return 0;
}


