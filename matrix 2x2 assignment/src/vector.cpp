#include <iostream>
using namespace std;

class matrix
{
private:
	int row;
	int col;
	int **arr;
public:
	matrix(void)
	{
		this->row=0;
		this->col=0;
		this->arr=NULL;
	}
	matrix(int size)
	{
		this->row=size;
		this->col=size;
		this->arr=new int*[row];
		for(int index=0;index<this->row;++index)
			this->arr[index]=new int[col];
	}
	void acceptRecord()
	{
		for(int r=0;r<this->row;++r)
		{
			for(int c=0;c<this->col;++c)
			{
				cout<<"enter element   :   ";
				cin>>this->arr[r][c];
			}
		}
	}
	void PrintRecord()
		{
			for(int r=0;r<this->row;++r)
			{
				for(int c=0;c<this->col;++c)
				{
					cout<<this->arr[r][c]<<" ";
				}
				cout<<endl;
			}
		}
	matrix& operator+(matrix &other)
	{
		for(int r=0;r<this->row;++r)
		{
			for(int c=0;c<this->col;++c)
			{
				int element=this->arr[r][c]+other.arr[r][c];
				this->arr[r][c]=element;
			}
		}
		return *this;
	}
	matrix& operator-(matrix &other)
		{
			for(int r=0;r<this->row;++r)
			{
				for(int c=0;c<this->col;++c)
				{
					int element=this->arr[r][c]-other.arr[r][c];
					this->arr[r][c]=element;
				}
			}
			return *this;
		}
	matrix& operator*(matrix &other)
		{
			for(int r=0;r<this->row;++r)
			{
				for(int c=0;c<this->col;++c)
				{
					int element=this->arr[r][c]*other.arr[r][c];
					this->arr[r][c]=element;
				}
			}
			return *this;
		}
	matrix& operator/(matrix &other)
		{
			for(int r=0;r<this->row;++r)
			{
				for(int c=0;c<this->col;++c)
				{
					int element=this->arr[r][c]/other.arr[r][c];
					this->arr[r][c]=element;
				}
			}
			return *this;
		}
};
int menu_list()
{
	int choice;
	cout<<endl;
	cout<<"0.exit"<<endl;
	cout<<"1.Addition"<<endl;
	cout<<"2.substraction"<<endl;
	cout<<"3.Multiplication"<<endl;
	cout<<"4.division"<<endl;
	cout<<"enter your choice"<<endl;
	cin>>choice;
	return choice;
}
int main()
{
	int choice;
	matrix m(3),n(3),m1;
	while((choice=menu_list())!=0)
	{
		switch(choice)
		{
		case 1:
			cout<<"enter element for 1st matrix"<<endl;
			m.acceptRecord();
			cout<<"enter element for 2st matrix"<<endl;
			n.acceptRecord();
			m1=m+n;
			cout<<"matrix Addition is     :   "<<endl;
			m1.PrintRecord();
			break;
		case 2:
			cout<<"enter element for 1st matrix"<<endl;
			m.acceptRecord();
			cout<<"enter element for 2st matrix"<<endl;
			n.acceptRecord();
			m1=m-n;
			cout<<"matrix substraction is     :   "<<endl;
			m1.PrintRecord();
			break;
		case 3:
			cout<<"enter element for 1st matrix"<<endl;
			m.acceptRecord();
			cout<<"enter element for 2st matrix"<<endl;
			n.acceptRecord();
			m1=m*n;
			cout<<"matrix multiplication is     :   "<<endl;
			m1.PrintRecord();
			break;
		case 4:
			cout<<"enter element for 1st matrix"<<endl;
			m.acceptRecord();
			cout<<"enter element for 2st matrix"<<endl;
			n.acceptRecord();
			m1=m/n;
			cout<<"matrix division is     :   "<<endl;
			m1.PrintRecord();
			break;
		}
	}

	return 0;
}
