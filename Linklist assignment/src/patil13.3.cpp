#include<iostream>
using namespace std;
class LinkList;
class Node
{
private:
	int data;
	Node *next;
public:
	Node()
  {
		this->data=0;
		this->next=NULL;
  }
	friend class LinkList;
};
class LinkList
{
private:
	Node *head;
public:
	LinkList()
	{
		this->head=NULL;
	}
	Node* CreateNode(int val)
	{
		Node *newnode=new Node();
		newnode->data=val;
		newnode->next=NULL;
		return newnode;
	}
	void AddFirst()
	{
		Node *newnode;
		int val;
		cout<<"enter value  :   ";
		cin>>val;
		newnode=CreateNode(val);
		if(this->head==NULL)
		{
			this->head=newnode;
		}
		else
		{
			newnode->next=this->head;
			this->head=newnode;
		}
	}
	void AddLast()
	{
		Node *trav;
		int val;
		cout<<"enter value  :   ";
		cin>>val;
		trav=this->head;
		Node *newnode=CreateNode(val);
		if(this->head==NULL)
		{
			this->head=newnode;
		}
		else
		{
			while(trav->next!=NULL)
			{
				trav=trav->next;
			}
			trav->next=newnode;
		}
	}
	void Display()
	{
		Node *trav;
		trav=this->head;
		while(trav!=NULL)
		{
			cout<<trav->data<<" ";
			trav=trav->next;
		}
		cout<<endl;
	}
	void DeleteFirst()
	{
		Node *temp;
		int val=0;
		if(this->head==NULL)
		{
			cout<<"List is empty  ::   "<<endl;
		}
		else
		{
			val=this->head->data;
			temp=this->head;
			this->head=this->head->next;
			free(temp);
			cout<<"value  :  "<<val;
		}
	}
	void DeleteAll()
	{
		Node *temp;
		while(this->head!=NULL)
		{
			temp=this->head;
			this->head=this->head->next;
			free(temp);
		}
		cout<<"list is empty"<<endl;
	}

};

int menu_list()
{
	int choice;
	cout<<endl;
	cout<<"0.exit"<<endl;
	cout<<"1.Add First"<<endl;
	cout<<"2.Add Last"<<endl;
	cout<<"3.Display"<<endl;
	cout<<"4.Delete First"<<endl;
	cout<<"5.Delete All"<<endl;
	cout<<"enter your choice"<<endl;
	cin>>choice;
	return choice;
}
int main()
{
	LinkList list;
	int choice;
	while((choice=menu_list())!=0)
	{

			switch(choice)
			{
			case 1:
			    list.AddFirst();
				break;
			case 2:
			    list.AddLast();
			break;
			case 3:
				list.Display();
				break;
			case 4:
				list.DeleteFirst();
				break;
			case 5:
				list.DeleteAll();
				break;
			}

	}
}

