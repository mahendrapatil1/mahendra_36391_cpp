#include<cstdlib>
#include<iostream>
#include<string>
using namespace std;
class IllegalArgumentException
{
private:
	string message;
public:
	IllegalArgumentException(const string message="Illegal Argument Exception")throw()
	{
		this->message=message;
	}
	string getmessage()const throw()
	{
		return this->message;
	}
};
template<typename T>
class Array
{
private:
	int size;
	T *arr;
public:
	Array()throw()
	{
		this->size=0;
		this->arr=NULL;
	}
	Array(int size)
	{	if(this->size<0)
			throw IllegalArgumentException("Invalid Size");
		this->size=size;
		this->arr=new int[this->size];
	}
	void acceptRecord()
	{
		for(int index=0;index<this->size;++index)
		{
			cout<<"arr["<<index<<"]   :   ";
			cin>>this->arr[index];
		}
	}
	void PrintRecord()
		{
			for(int index=0;index<this->size;++index)
			{
				cout<<"arr["<<index<<"]   :   "<<this->arr[index]<<endl;
			}
		}
	~Array()
	{
		if(this->arr!=NULL)
		{
			delete[] this->arr;
			this->arr=NULL;
		}
	}
};
template<typename T>
class AutoPointer
{
private:
        Array<T> *ptr;
public:
        AutoPointer( Array<T> *ptr )
        {
                this->ptr = ptr;
        }
        Array<T>* operator->( )
        {
                return this->ptr;
        }
        ~AutoPointer( void )
        {
                delete this->ptr;
        }
};

int menu_list()
{
	int choice;
	cout<<endl;
	cout<<"1.accept Record"<<endl;
	cout<<"2.print Record"<<endl;
	cout<<"enter choice";
	cin>>choice;
	return choice;
}

int main()
{
	AutoPointer<int>obj( new Array<int>( 3 ) );
	int choice;
	while((choice=menu_list()))
	{
		try
		{
			switch(choice)
			{
			case 1:
				obj->acceptRecord();
				break;
			case 2:
				obj->PrintRecord();
				break;
			}
		}
		catch(IllegalArgumentException &ex)
		{
			cout<<ex.getmessage();
		}
	}


	return 0;
}
